## Person Background API

This API simulates a personal background verification system. It is used to verify
if the person has any bad reports with a security agency. 

### Install the following tools

- jdk 1.8
- maven 3.5

### Getting Started

This is a spring boot application wrapped as maven module. So, after cloning the repository,
please go to the project root folder and execute the following commands:

- Generate the binary distribution

`$ mvn clean package`

- Run the application

```
$ cd target
$ java -jar person-bg-0.0.1-SNAPSHOT.jar

```

The above will run the mock api in port [1985](http://localhost:1985)