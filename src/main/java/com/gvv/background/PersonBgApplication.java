package com.gvv.background;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PersonBgApplication {

    public static void main(String[] args) {
        SpringApplication.run(PersonBgApplication.class, args);
    }

}
