package com.gvv.background;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
@RequestMapping("/person-api/v1")
public class PersonController {

    private Set<String> blackList = new HashSet<>();

    public PersonController(){
        blackList.add("123");
        blackList.add("456");
    }

    @GetMapping("/person/{publicId}/background")
    public ResponseEntity<PersonDTO> getPersonBackground(@PathVariable String publicId){
        return blackList.stream()
            .filter(element -> element.equalsIgnoreCase(publicId))
            .findAny()
            .map(element -> ResponseEntity.ok().body(new PersonDTO(publicId, true)))
            .orElseGet(() -> ResponseEntity.ok().body(new PersonDTO(publicId, false)));
    }
}
