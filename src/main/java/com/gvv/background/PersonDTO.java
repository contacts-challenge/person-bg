package com.gvv.background;

public class PersonDTO {
    private String publicId;
    private Boolean background;

    public PersonDTO() {
    }

    public PersonDTO(String publicId, Boolean background) {
        this.publicId = publicId;
        this.background = background;
    }

    public String getPublicId() {
        return publicId;
    }

    public void setPublicId(String publicId) {
        this.publicId = publicId;
    }

    public Boolean getBackground() {
        return background;
    }

    public void setBackground(Boolean background) {
        this.background = background;
    }
}
